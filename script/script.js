const firstName = prompt('Enter your name');
const lastName = prompt('Enter your lastname');
const age = prompt('Enter your birthday in format dd.mm.yyyy');

function createNewUser(firstName, lastName, age){
  return {
    userName: firstName,
    userLastName: lastName,
    birthday: age,
    getLogin (){
      return this.userName[0].toLowerCase() + this.userLastName[0].toUpperCase() + this.userLastName.slice(1).toLowerCase();
    },
    getPassword (){
      return this.userName[0].toUpperCase() + this.userLastName.toLowerCase() + this.birthday.slice(-4);
    },
    getAge () {
      const userAge = Date.parse(age.split('.').reverse().join('-'));
      const nowDateTime = Date.now();
      const timeInMs = 1000*3600*24*365;
      return Math.floor((nowDateTime - userAge) / timeInMs);
    },
  }
}

const newUser = createNewUser(firstName, lastName, age);
console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());


